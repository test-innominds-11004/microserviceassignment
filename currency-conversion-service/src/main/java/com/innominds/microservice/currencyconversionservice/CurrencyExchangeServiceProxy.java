package com.innominds.microservice.currencyconversionservice;
@FeignClient(name="currency-exchange-service", url="localhost:8000")

public interface CurrencyExchangeServiceProxy {
	@GetMapping("/currency-exchange/from/{from}/to/{to}")
	public CurrencyConversionBean retrieveExchangeValue
		(@PathVariable("from") String from, @PathVariable("to") String to);
}
